const App = getApp();
Page({
	data: {
		isThrowing: false,
		scrollContent: '',
	},
	throwScroll() {
		this.setData({
			isThrowing: true,
		})
	},
	onInput(event) {
		this.setData({
			scrollContent: event.detail.value,
		})
	},
	throwFinish() {
		var that = this;
		let userInfo = wx.getStorageSync("chat-mini-program-userInfo");
		wx.request({
			url: App.globalData.baseAPI + "mobile/message/throwHole",
			data: {
				openId: App.globalData.userInfo.openId,
				message: that.data.scrollContent,
				gender: App.globalData.userInfo.gender
			},

			method: "post",
			header: {
				'content-type': 'application/json'
			},
			success: function(res) {
				if (res.data.code == 0) {
					that.setData({
						isThrowing: false,
						scrollContent: '',
					})
					wx.showToast({
						title: '你的秘密已经被扔进了树洞',
						icon: 'success',
						duration: 2000,
					})
				}
			}
		})

	},
})