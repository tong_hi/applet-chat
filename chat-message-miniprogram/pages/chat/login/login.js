// pages/chat/login/login.js
var app = getApp()

Page({

	/**
	 * 页面的初始数据
	 */
	data: {

	},
	async getUserProfile(e) {
		var that = this;
		wx.login({
			success: function(res) {
				if (res.code) {
					wx.request({
						url: app.globalData.baseAPI + '/mobile/wxlogin/getOpenIdByCode',
						data: {
							code: res.code
						},
						success: function(res) {
							if (res.data.code == '0') {
								app.globalData.userInfo = res.data.data; // 将用户信息存到globalData里面
								 wx.setStorageSync("chat-mini-program-userInfo",JSON.stringify(app.globalData.userInfo))
								// 登录成功，跳转到首页
								wx.showToast({
									title: '登录成功',
									icon: 'none'
								});
								wx.switchTab({
								  url: '/pages/chat/userList',
								  success: function() {
								    console.log('跳转成功');
								  },
								  fail: function() {
								    console.log('跳转失败');
								  }
								})
							} else {
								// 登录失败，提示用户
								wx.showToast({
									title: '登录失败，请重试',
									icon: 'none'
								});
							}
						},
						fail: function() {
							// 请求失败，提示用户
							wx.showToast({
								title: '网络错误，请检查您的网络',
								icon: 'none'
							});
						}
					});
				} else {
					// 获取 code 失败，提示用户
					wx.showToast({
						title: '登录失败，请重试',
						icon: 'none'
					});
				}
			},
			fail: function() {
				// 登录失败，提示用户
				wx.showToast({
					title: '登录失败，请重试',
					icon: 'none'
				});
			}
		});
		/*try {
			// 获取用户授权信息
			const {
				userInfo
			} = await wx.getUserInfo({
				withCredentials: true
			})
			console.log(userInfo)
			// TODO: 处理用户信息
			this.setData({
				hasUserInfo: true
			})
			var app = getApp()
			app.globalData.userInfo = userInfo; // 将用户信息存到globalData里面
			this.linkUserList();
		} catch (error) {
			console.error(error)
		}*/
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad(options) {
		var app = getApp()
		if (app.globalData.userInfo != null) {
			wx.switchTab({
			  url: '/pages/chat/userList',
			  success: function() {
			    console.log('跳转成功');
			  },
			  fail: function() {
			    console.log('跳转失败');
			  }
			})
		}
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage() {

	}
})