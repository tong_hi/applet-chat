package com.chat.message.web;

import com.chat.message.service.UploadService;
import com.chat.message.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author th
 * @date 2020/12/11 图片上传
 **/

@Api(tags = "图片上传")
@RestController
@RequestMapping("/hole/oss")
@AllArgsConstructor
public class UploadController {

	private final UploadService uploadService;

	/**
	 * 上传文件 文件名采用uuid,避免原始文件名中带"-"符号导致下载的时候解析出现异常
	 * @param file 资源
	 * @return R(/ admin / bucketName / filename)
	 */
	@PostMapping("/uploads")
	@ApiOperation(value = "需要登陆；上传头像")
	public R upload(@RequestParam("file") MultipartFile file) {
		Map<String, String> filList = uploadService.uploadOneFile(file);
		return R.ok(filList);
	}

	/**
	 * 删除上传文件
	 * @param fileName 文件名称
	 * @return R
	 */
	@GetMapping("/remove")
	public R remove(@RequestParam(value = "fileName") String fileName) {
		uploadService.remove(fileName);
		return R.ok();
	}

}
