package com.chat.message.web;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chat.message.model.HoleMessageEntity;
import com.chat.message.model.User;
import com.chat.message.model.request.HoleMessageInsertRequest;
import com.chat.message.service.HoleMessageService;
import com.chat.message.service.UserService;
import com.chat.message.util.BeanCopierUtil;
import com.chat.message.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lisw
 * @program hole
 * @description
 * @createDate 2021-08-18 17:08:18
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
@RestController
@RequestMapping("/mobile/message")
@Slf4j
public class HoleMessageController {

    @Autowired
    private UserService userService;

    @Autowired
    private HoleMessageService holeMessageService;

    @PostMapping("/throwHole")
    public R throwHoleMessage(@RequestBody HoleMessageInsertRequest holeMessageInsertRequest){
        User user = userService.getOne(Wrappers.lambdaQuery(User.class)
                .eq(User::getOpenId,holeMessageInsertRequest.getOpenId()));
        //将这个信息存入表中
        HoleMessageEntity holeMessageEntity = BeanCopierUtil.beanCopy(holeMessageInsertRequest, HoleMessageEntity.class);
        holeMessageEntity.setThrowUserId(user.getId());
        return R.ok(holeMessageService.save(holeMessageEntity));
    }


}
