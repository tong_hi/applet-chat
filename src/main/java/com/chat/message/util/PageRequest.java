package com.chat.message.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 分页请求类
 *
 * @author 吕恒吉
 * @date 2017/10/29
 */
@Data
@ApiModel("分页请求对象")
public class PageRequest< T > {
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true, example = "1")
    @NotNull(message = "当前页不能为空")
    private Integer pageNum;

    /**
     * 每页数量
     */
    @ApiModelProperty(value = "每页数量", required = true, example = "10")
    @NotNull(message = "每页数量不能为空")
    private Integer pageSize;

    /**
     * 分页请求对象参数
     */
    @Valid
    private T data;


    public Page getPage(){
        return  new Page(this.pageNum,this.pageSize);
    }
}
