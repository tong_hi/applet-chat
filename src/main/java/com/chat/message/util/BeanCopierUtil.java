package com.chat.message.util;

import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.beans.BeanCopier;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * BeanCopier工具类
 *
 * @author 吕恒吉
 * @date 2020/8/15 16:02
 */
@Slf4j
public class BeanCopierUtil {

    /**
     * 构造器私有，防止外部实例化
     */
    private BeanCopierUtil() {
    }

    /**
     * BeanCopier缓存
     */
    private static final ConcurrentHashMap<String, BeanCopier> BEAN_COPIER_CACHE = new ConcurrentHashMap<>();

    /**
     * bean对象拷贝
     * @param source 源对象
     * @param targetClazz 目标对象
     * @param <T> 目标类泛型参数
     * @return 目标对象
     */
    public static < T > T beanCopy(Object source, Class < T > targetClazz) {
        String key = source.getClass().getName() + targetClazz.getName();
        BeanCopier beanCopier;
        if (BEAN_COPIER_CACHE.containsKey(key)) {
            beanCopier = BEAN_COPIER_CACHE.get(key);
        } else {
            beanCopier = BeanCopier.create(source.getClass(), targetClazz, false);
            BEAN_COPIER_CACHE.put(key, beanCopier);
        }
        T target = ReflectUtil.newInstanceIfPossible(targetClazz);
        beanCopier.copy(source, target, null);
        return target;
    }

    /**
     * 集合拷贝
     * @param sourceList 源集合
     * @param clazz 目标类对象
     * @param <T1> 源集合泛型参数
     * @param <T2> 目标类泛型参数
     * @return 目标集合
     */
    public static <T1, T2> List<T2> listCopy(List<T1> sourceList, Class<T2> clazz) {
        return sourceList.stream().map(t1 -> beanCopy(t1, clazz)).collect(Collectors.toList());
    }

    /**
     * Page对象拷贝
     *
     * @param sourcePage 源分页对象
     * @param clazz 目标类对象
     * @param <T1> 源集合泛型参数
     * @param <T2> 目标类泛型参数
     * @return 拷贝的分页对象
     */
    public static < T1, T2 > PageData< T2 > pageCopy(Page< T1 > sourcePage, Class < T2 > clazz) {
        List < T2 > targetList = listCopy(sourcePage.getRecords(), clazz);
        PageData < T2 > targetPage = new PageData <>(targetList,targetList.size());
        targetPage.setTotal(sourcePage.getTotal());
        targetPage.setList(targetList);
        return targetPage;
    }
}
