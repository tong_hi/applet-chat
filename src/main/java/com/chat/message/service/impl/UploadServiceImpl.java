package com.chat.message.service.impl;

import com.chat.message.config.oss.OssProperties;
import com.chat.message.config.oss.OssTemplate;
import com.chat.message.service.UploadService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * @author zengjiayong
 * @date 2020/12/17
 **/
@Service
@Slf4j
@AllArgsConstructor
public class UploadServiceImpl implements UploadService {

	private final OssProperties ossProperties;

	private final OssTemplate minioTemplate;


	@Override
	public List<Map<String, String>> uploads(List<MultipartFile> file) {
		List<Map<String, String>> filList = new ArrayList();
		for (MultipartFile Map : file) {
			Map<String, String> resultMap = uploadOneFile(Map);
			filList.add(resultMap);
		}
		return filList;
	}

	@Override
	public void remove(String fileName) {
		String[] taxation = fileName.substring(fileName.indexOf(ossProperties.getBucketName())).split("/");
		try {
			minioTemplate.removeObject(ossProperties.getBucketName(), taxation[1]);
		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
		}
	}

	@Override
	public Map<String, String> uploadOneFile(MultipartFile file) {
		if ((file.getSize()/1024/1024) >= ossProperties.getMaxFileSize()) {
			throw new MultipartException(file.getOriginalFilename() + " 文件不能大于" + ossProperties.getMaxFileSize() + "M");
		}
		String subString = file.getOriginalFilename().equals("")?".jpg":file.getOriginalFilename();
		String fileName = UUID.randomUUID().toString().replaceAll("-", "")
				+ subString.substring(subString.lastIndexOf("."));
		Map<String, String> resultMap = new HashMap<>(4);
		resultMap.put("fileSize",String.valueOf(file.getSize()));
		resultMap.put("bucketName", ossProperties.getBucketName());
		resultMap.put("fileName",fileName);
		resultMap.put("original", file.getOriginalFilename());
		resultMap.put("type",file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1));
		//先取出拼接的 %s/ ossProperties.getEndpoint()
		resultMap.put("url",
				String.format("%s/%s/%s",ossProperties.getEndpoint(), ossProperties.getBucketName(), fileName));
		try {
			minioTemplate.putObject(ossProperties.getBucketName(), fileName, file.getInputStream());
		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
		}
		return resultMap;
	}

}
