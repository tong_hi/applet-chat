package com.chat.message.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author markeloff
 * @date 2020/12/17
 **/
public interface UploadService {

	/**
	 * 上传文件 文件名采用uuid,避免原始文件名中带"-"符号导致下载的时候解析出现异常
	 * @param file 资源
	 * @return R(/ admin / bucketName / filename)
	 */
	List<Map<String, String>> uploads(List<MultipartFile> file);

	/**
	 * 删除上传文件
	 * @param fileName 文件名称
	 * @return R
	 */
	void remove(String fileName);

	Map<String, String> uploadOneFile(MultipartFile file);

}
